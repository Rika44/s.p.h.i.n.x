from email.errors import MessageParseError
import sys
import socket
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

MESSAGE_SIZE = 1024
SERVER_IP = "127.0.0.1"
SERVER_PORT = 1306
CODES = {
    "LOGIN": "200",
    "SIGNUP": "210",
    "SEND_VIDEO": "220",
    "QUIT": "230"
}
#SERVER_CODES
SOCK = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

class select_video_window(QMainWindow):
    def __init__(self):
        super(select_video_window,self).__init__()
        self.initUI()
    def initUI(self):
        self.setWindowTitle('Sphinx- Select Video Window')
        self.setGeometry(800, 400, 1100, 600)
        #Label
        self.label = QLabel(self)
        self.label.setText("Please select a video")
        self.label.resize(200, 35)
        self.label.move(450, 200)
        #Button
        self.button = QPushButton(self)
        self.button.setText("import")
        self.button.resize(150, 35)
        self.button.move(475, 350)
        self.button.clicked.connect(self.send_video)
    def file_dialog(self):
        return QFileDialog.getOpenFileName(self, 'Open file', r'C:\\',"MP4 files (*.mp4)")[0]
    def send_video(self):
        SOCK.send(create_message("SEND_VIDEO").encode())
        with open(self.file_dialog(), "rb") as video:
            buffer = video.read()
            [SOCK.send(buffer[i: i + MESSAGE_SIZE]) for i in range(0, len(buffer), MESSAGE_SIZE)]
            SOCK.send("Finished".encode())
        self.msg = QMessageBox(self)    
        self.msg.setIcon(QMessageBox.Information)
        self.msg.setText(SOCK.recv(MESSAGE_SIZE).decode())
        self.msg.setWindowTitle('Result')
        self.msg.resize(100, 20)
        self.msg.exec_()



class login_window(QDialog):
    def __init__(self, parent=None):
        super(login_window, self).__init__(parent)
        self.initUI()
    def initUI(self):
        self.setWindowTitle('Sphinx- Login Window')
        self.setGeometry(800, 400, 1100, 600)
        #Label- Username
        self.Lusername = QLabel(self)
        self.Lusername.setText('Username')
        self.Lusername.setFont(QFont('Sriracha', 9))
        self.Lusername.move(250, 100)
        self.Lusername.resize(150, 37) 
        #Text Box- Username
        self.LEusername = QLineEdit(self)
        self.LEusername.setFont(QFont('Sriracha', 9))
        self.LEusername.setAlignment(Qt.AlignCenter)
        self.LEusername.move(500, 100)     
        self.LEusername.resize(300, 37) 
        #Label- Password
        self.Lpassword = QLabel(self)
        self.Lpassword.setText('Password')
        self.Lpassword.setFont(QFont('Sriracha', 9))
        self.Lpassword.move(250, 200)
        self.Lpassword.resize(150, 37) 
        #Text Box- Password
        self.LEpassword = QLineEdit(self)
        self.LEpassword.setFont(QFont('Sriracha', 9))
        self.LEpassword.setAlignment(Qt.AlignCenter)
        self.LEpassword.move(500, 200) 
        self.LEpassword.resize(300, 37)  
        #Button- Login
        self.Blogin = QPushButton(self)
        self.Blogin.setText('Login')
        self.Blogin.setFont(QFont('Sriracha', 8))
        self.Blogin.clicked.connect(self.check_password)
        self.Blogin.move(400, 330)
        self.Blogin.resize(200, 35)  
        #Button- Signup
        self.Bsignup = QPushButton(self)
        self.Bsignup.setText("Don't have an account? Sign up")
        self.Bsignup.setFont(QFont('Sriracha', 8))
        self.Bsignup.clicked.connect(self.signup_dialog)
        self.Bsignup.move(300, 400)
        self.Bsignup.resize(400, 35)
    def check_password(self):
        SOCK.send(create_message("LOGIN", self.LEusername.text(), self.LEpassword.text()).encode())
        if "120" in SOCK.recv(MESSAGE_SIZE).decode():
            self.accept()
        else:
            self.msg = QMessageBox(self)    
            self.msg.setIcon(QMessageBox.Critical)
            self.msg.setText('Bad username or password')
            self.msg.setWindowTitle('Error')
            self.msg.exec_()
    def signup_dialog(self):
        self.signup = signup_window()
        self.hide()
        if self.signup.exec_() == QDialog.Accepted:
            self.accept()



class signup_window(QDialog):
    def __init__(self):
        super(signup_window,self).__init__()
        self.initUI()
    def initUI(self):
        self.setWindowTitle('Sphinx- Signup Window')
        self.setGeometry(800, 400, 1100, 600)
        #Label- Email
        self.Lemail = QLabel(self)
        self.Lemail.setText('Email')
        self.Lemail.setFont(QFont('Sriracha', 9))
        self.Lemail.move(250, 100)
        self.Lemail.resize(150, 37) 
        #Text Box- Email
        self.LEemail = QLineEdit(self)
        self.LEemail.setFont(QFont('Sriracha', 9))
        self.LEemail.setAlignment(Qt.AlignCenter)
        self.LEemail.move(500, 100)     
        self.LEemail.resize(300, 37) 
        #Label- Username
        self.Lusername = QLabel(self)
        self.Lusername.setText('Username')
        self.Lusername.setFont(QFont('Sriracha', 9))
        self.Lusername.move(250, 200)
        self.Lusername.resize(150, 37) 
        #Text Box- Username
        self.LEusername = QLineEdit(self)
        self.LEusername.setFont(QFont('Sriracha', 9))
        self.LEusername.setAlignment(Qt.AlignCenter)
        self.LEusername.move(500, 200)     
        self.LEusername.resize(300, 37) 
        #Label- Password
        self.Lpassword = QLabel(self)
        self.Lpassword.setText('Password')
        self.Lpassword.setFont(QFont('Sriracha', 9))
        self.Lpassword.move(250, 300)
        self.Lpassword.resize(150, 37) 
        #Text Box- Password
        self.LEpassword = QLineEdit(self)
        self.LEpassword.setFont(QFont('Sriracha', 9))
        self.LEpassword.setAlignment(Qt.AlignCenter)
        self.LEpassword.move(500, 300) 
        self.LEpassword.resize(300, 37)  
        #Button- Signup
        self.Bsignup = QPushButton(self)
        self.Bsignup.setText("Sign up")
        self.Bsignup.setFont(QFont('Sriracha', 8))
        self.Bsignup.clicked.connect(self.check_details)
        self.Bsignup.move(400, 400)
        self.Bsignup.resize(200, 35)
    def check_details(self):
        self.accept()
        SOCK.send(create_message("SIGNUP", self.LEusername.text(), self.LEpassword.text(), self.LEemail.text()).encode())
        if "100" in SOCK.recv(MESSAGE_SIZE).decode():
            self.accept()
        else:
            self.msg = QMessageBox(self)    
            self.msg.setIcon(QMessageBox.Critical)
            self.msg.setText('Error- Please try again')
            self.msg.setWindowTitle('Error')
            self.msg.exec_()


def create_message(message_type, username = "", password = "", email = ""):
    switcher = {
        "LOGIN": "&&".join((CODES[message_type], username, password)),
        "SIGNUP": "&&".join((CODES[message_type], username, password, email)),
        "SEND_VIDEO": CODES[message_type],
        "QUIT": CODES[message_type]
    }
    return switcher.get(message_type, CODES["QUIT"])



def main():
    server_address = (SERVER_IP, SERVER_PORT)
    SOCK.connect(server_address)
    app = QApplication(sys.argv)
    login = login_window()
    if login.exec_() == QDialog.Accepted:
        window = select_video_window()
        window.show()
        sys.exit(app.exec_())

if __name__ == '__main__':
    main()
