import cv2
import argon2
import numpy as np
import socket
from tensorflow import keras
from keras import backend as K
import mysql.connector

#global varibles
SIZE = 224
LISTEN_PORT = 1306
NUM_OF_FRAMES = 10
SERVER_ADDRESS = "127.0.0.1"
LABELS_OPTIONS = ["Normal", "Shoplifting"]
MODEL_PATH = r"C:\Sphinx\3_Model\3_Model\Model"
NEW_VIDEO_PATH = r"C:\Sphinx\\new_video.mp4"
NEW_VIDEO_WRITER_PATH = r"C:\Sphinx\\new_video1.mp4"
CLIENTS_NUMBER = 5

CODES = {
    "LOGIN": "200",
    "SIGNUP": "210",
    "SEND_VIDEO": "220",
    "QUIT": "230"
}

"""
global varible to connect the DB.
"""
db = mysql.connector.connect(
  host="localhost",
  user="root",
  password="Rr214673444",
  database="sphinx"
)


sqlCreateTable = '''CREATE TABLE USERS(
    userid INT,
    username TEXT,
    password TEXT,
    email TEXT,
    PRIMARY KEY(userid)
 )'''


"""
This function will turn the video
to array of frames in numpy format
that the neural network can read.
input: original video.
output: numpy array.
"""
def video_to_dataset():
    data = []
    index = 0
    video = cv2.VideoCapture(NEW_VIDEO_PATH)
    count_of_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    numpy_of_frames = np.empty((NUM_OF_FRAMES, SIZE, SIZE), np.dtype('uint8'))
    print(count_of_frames)
    for n in range(count_of_frames//NUM_OF_FRAMES):
        ret, image = video.read()
        image = cv2.resize(image, (SIZE, SIZE), fx=0, fy=0, interpolation=cv2.INTER_CUBIC) #Cut the frame (size) -> 224, 224
        numpy_of_frames[index] = np.array(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
        index += 1
        if index == NUM_OF_FRAMES:
            data.append(numpy_of_frames)
            numpy_of_frames = np.empty((NUM_OF_FRAMES, SIZE, SIZE), np.dtype('uint8')) #New numpy
            index = 0
    video.release()
    return np.asarray(data) #List -> Numpy


"""
This function will check if the 
data base table of users is already exist.
input: the mySQL connection.
output: exist or not.
"""
def check_if_table_exist(sphinxDB):
    sphinxDB.execute("SHOW TABLES")
    for x in sphinxDB:
        if x == "USERS":
            return 1
    return 0


"""
This function will get the video from the user
and send the video to other function that
turn the video into format that the neural netowrk
can read.
input: connection.
output: none.
"""
def get_video(connection, model):
    print(connection)
    buffer = connection.recv(1024)
    with open(NEW_VIDEO_PATH, "wb") as video:
        while ("Finished").encode() not in buffer:
            video.write(buffer)
            buffer = connection.recv(1024)
    print("finished")
    labels = get_labels(video_to_dataset(), model)
    count_normal = labels.count("Normal")
    count_shoplifting = labels.count("Shoplifting")
    return ("Normal" if count_normal > count_shoplifting else "Shoplifting")


"""
This function will get the 
msg of login and check if the password and user name
is right.
input: msg from user.
output: none.
"""
def connect_user(msg, sphinx, ph):
    i = 0
    checkIfEmpty = "SELECT EXISTS(select 1 from USERS) AS Output;"
    sphinx.execute(checkIfEmpty)
    output = sphinx.fetchone()
    if output[0] != 1:
        print("errorONE")
        return "420"
    msg = msg[5:]
    userName = msg.split("&&")[0]
    password = msg.split("&&")[1]
    checkIfuserExists = "SELECT userName FROM USERS;"
    sphinx.execute(checkIfuserExists)
    output = sphinx.fetchall()
    while i <= len(output) -1:
        if userName in output[i]:
            checkMatch = "SELECT password FROM USERS WHERE username = '" + userName + "';"
            sphinx.execute(checkMatch)
            output = sphinx.fetchall()
            try:
                result = ph.verify(output[0][0], password)
            except argon2.exceptions.VerifyMismatchError:
                result = False
            if result == True:
                return "120"
        i += 1
    return "420"


"""
This function will get the
msg of signup and join the new user
into the data base.
input: msg from user.
output: none.
"""
def new_user(msg, sphinx, ph):
    msg = msg[5:] #without the code
    userName = msg.split("&&")[0]
    password = msg.split("&&")[1]
    encryptedPass = ph.hash(password)
    email = msg.split("&&")[2]
    checkIfEmpty = "SELECT EXISTS(select 1 from USERS) AS Output;"
    sphinx.execute(checkIfEmpty)
    output = sphinx.fetchone()
    if output[0] != 1:
        userID = 1
    else:
        getUserID = "SELECT MAX(userid) AS maximum FROM USERS"
        sphinx.execute(getUserID)
        output = sphinx.fetchone()
        userID = output[0] + 1
    addToTable = "INSERT INTO USERS (userid, username, password, email) VALUES (%s, %s, %s, %s);"
    val = (userID, userName, encryptedPass, email)
    sphinx.execute(addToTable, val)
    return "100"


"""
This function will get the labels from
the neural network.
input: data, model.
output: labels.
"""
def get_labels(data, model):
    labels = []
    pred = model.predict(data)
    y_pred = np.argmax(pred, axis=1)
    [labels.extend([LABELS_OPTIONS[pred]] * NUM_OF_FRAMES) for pred in y_pred] #List of labels, one per frame
    return labels

"""
This function will get the labels
and put each labels to each frame
and save the new video.
input: labels, video.
output: none.
"""
def new_video(labels, video_reader):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video_writer = cv2.VideoWriter('C:\Sphinx\\new_video1.mp4', fourcc, float(15), (SIZE, SIZE))
    print(labels)
    for label in labels:
        ret, image = video_reader.read()
        image = cv2.resize(image, (SIZE, SIZE), fx=0, fy=0, interpolation=cv2.INTER_CUBIC) #Cut the frame -> 224, 224
        cv2.putText(image, label, (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (182, 132, 95), 2, cv2.LINE_4)
        video_writer.write(image)
    video_reader.release()
    video_writer.release()

def main():
    def recall_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall
    def precision_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision
    def f1_m(y_true, y_pred):
        precision = precision_m(y_true, y_pred)
        recall = recall_m(y_true, y_pred)
        return 2*((precision*recall)/(precision+recall+K.epsilon()))
    model = keras.models.load_model(MODEL_PATH, custom_objects={'f1_m':f1_m, 'precision_m':precision_m, 'recall_m':recall_m})
    #DB:
    sphinx = db.cursor(buffered=True) #connect the db
    if check_if_table_exist(sphinx):
        sphinx.execute(sqlCreateTable)
    #password encryption
    ph = argon2.PasswordHasher()
    #Socket:
    listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listening_sock.bind((SERVER_ADDRESS, LISTEN_PORT))
    listening_sock.listen(CLIENTS_NUMBER)
    print("Listening for connections...")
    connection, addr = listening_sock.accept()
    while True:
        msg = connection.recv(1024).decode()
        msg_code = msg[0:3]
        if msg_code == CODES["LOGIN"]:
            code = connect_user(msg, sphinx, ph)
            connection.send(code.encode())
        elif msg_code == CODES["SIGNUP"]:
            code = new_user(msg, sphinx, ph)
            connection.send(code.encode())
            db.commit()
        elif msg_code == CODES["SEND_VIDEO"]:
            code = get_video(connection, model)
            connection.send(code.encode())
        elif msg_code == CODES["QUIT"]:
            msgForClient = "goodBye!"
            connection.send(msgForClient.encode())
            break
        else:
            msgForClient = "error"
            connection.send(msgForClient.encode())
    connection.close()


if __name__ == '__main__':
    main()
